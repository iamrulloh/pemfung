data Expr = C Float | Expr :+ Expr | Expr :- Expr
           | Expr :* Expr | Expr :/ Expr 
           | V String
           | Let String Expr Expr      
      deriving Show

-- >>> (C 10.0 :+ (C 8 :/ C 2)) :* (C 7 :- C 4)
-- (C 10.0 :+ (C 8.0 :/ C 2.0)) :* (C 7.0 :- C 4.0)
--

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)          = (C c)
subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

eval :: Expr -> Float
eval (C x) = x
eval (e1 :+ e2)    = eval e1 + eval e2
eval (e1 :- e2)    = eval e1 - eval e2
eval (e1 :* e2)    = eval e1 * eval e2
eval (e1 :/ e2)    = eval e1 / eval e2
eval (Let v e0 e1) = eval (subst v e0 e1)
eval (V v)         = 0.0 

foldExpr (c,p,mi,mul,sub,v) (C x) = c x
foldExpr (c,p,mi,mul,sub,v) (e1 :+ e2) = p e1 e2
foldExpr (c,p,mi,mul,sub,v) (e1 :- e2) = mi e1 e2
foldExpr (c,p,mi,mul,sub,v) (e1 :* e2) = mul e1 e2
foldExpr (c,p,mi,mul,sub,v) (e1 :/ e2) = sub e1 e2
foldExpr (c,p,mi,mul,sub,v) (V x) = v x

newEval expr = foldExpr (ec, ep, emi, emul, esub, ev) expr
      where ec x = x
            ep n = n
            emi n = n
            emul n = n
            esub n = n
            ev x = 0



-- >>> eval((C 10.0 :+ (C 8 :/ C 2)) :* (C 7 :- C 4))
-- 42.0
-- 
-- >>> eval(Let "x" (C 7) ((C 5 :+ C 2 :* V "x")))
-- 49.0
--
-- >>> exp1 = Let "x" (C 7) ((C 5) :+ (C 7) :* (V "x"))
-- >>> exp1
-- >>> eval exp1
-- Let "x" (C 7.0) ((C 5.0 :+ C 7.0) :* V "x")
-- 84.0
--
