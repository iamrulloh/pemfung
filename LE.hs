import Data.List

pythagoras = [(x,y,z) | z <- [5 ..], y <- [1 .. z-1], x <- [1 .. y-1], x*x + y*y == z*z ]

perms [] = [[]]
perms ls = [ x:ps | x <- ls, ps <- perms (ls \\ [x]) ]

-- >>> perms [1,2,3]
-- [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
--

quickSort []      = []
quickSort [x]     = [x]
quickSort (x:xs)  = quickSort (filter (<=x) xs) ++ [x] ++ quickSort (filter (>x) xs)

-- >>> quickSort [5,1,4,2,6,9]
-- [1,2,4,5,6,9]
--


