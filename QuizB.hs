fpb x 0 = x
fpb x y = fpb y (x `mod` y)

kpk x y = [z | z <- [x,x*2..], z `mod` y == 0]!!0

-- >>> kpk 5 12
-- 60
--
-- >>> fpb 5 12
-- 1
--

maxList [] = 0
maxList (x:xs) = max x (maxList xs)

-- >>> maxList [2,9,1,3]
-- 9
--

merge [] kanan = kanan
merge kiri [] = kiri
merge kiri@(x:xs) kanan@(y:ys) | x<y = x : merge xs kanan
                               | otherwise = y : merge kiri ys

mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = merge (mergeSort kiri) (mergeSort kanan)
        where kiri = take ((length xs) `div` 2) xs
              kanan = drop ((length xs) `div` 2) xs

-- >>> mergeSort [1,5,6,9,0]
-- [0,1,5,6,9]
--

myConcat xs = foldl (++) [] xs
sumPair (x, y) = x+y

--- >>> myConcat [[2,3,4], [5,1,2]]
--- [2,3,4,5,1,2]
---
-- >>> sumPair (5, 7)
-- 12
--

myFlip f x y = f y x

-- >>> :t myFlip
-- myFlip :: (t1 -> t2 -> t3) -> t2 -> t1 -> t3
--

pythagoras = [(x,y,z) | z <- [5..], y <- [1..z-1], x <- [1..y-1], x*x + y*y == z*z]