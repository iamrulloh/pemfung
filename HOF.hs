lenofL xs = sum(map (\x -> 1) xs)
-- >>> lenofL [2,9,8]
-- 3
--

-- >>> map (+1) (map (+1) [1,2,3])
-- [3,4,5]
--
-- Fungsi map f (map g xs) dimana f dan g adalah fungsi yang berubah-ubah
-- akan melakukan operasi terhadap xs sesuai fungsi f g yang diberikan, fungsi g
-- pada map akan dieksekusi pertama kali dan kemudian diikuti oleh fungsi f.

iter 0 f x = x
iter n f x = f $ iter (n-1) f x
-- >>> iter 3 (+3) 3 
-- 12
--

sumSquare n = foldr (+) 0 (map (\x -> x**2) [1..n])
-- >>> sumSquare 4
-- 30.0
--

mystery xs = foldr (++) [] (map sing xs)
    where sing x = [x]
-- >>> mystery [1,5,2]
-- [1,5,2]
-- Mengembalikan list yang sama dengan input, hanya saja
-- saat fungsi dijalankan fungsi map sing xs akan memetakan
-- setiap elemen menjadi sebuah list baru, mis. [1,5,2] > [[1],[5],[2]]
-- lalu akan dilanjutkan dengan mengeksekusi fungsi foldr untuk setiap elemen
-- [5] ++ [2] menjadi [5,2] kemudian [1] ++ [2,5] menjadi [1,5,2]
-- hingga mencapai base case [1,5,2] ++ [] menjadi [1,5,2]

flip :: (a -> b -> c) -> (b -> a -> c)
flip f x y = f y x
-- >>> flip div 5 20
-- 4
--

oneList xs = [ x+1 | x <- xs ]
one xs = map (+1) xs
-- >>> one [1,2,3]
-- >>> oneList [1,2,3]
-- [2,3,4]
-- [2,3,4]
--


twoList xs ys = [ x+y | x <- xs, y <-ys ]
two xs ys = concat $ map (\x -> map (\y -> x+y) ys) xs 
-- >>> two [1,2,3] [4,5,6]
-- >>> twoList [1,2,3] [4,5,6]
-- [5,6,7,6,7,8,7,8,9]
-- [5,6,7,6,7,8,7,8,9]
--

threeList xs = [ x+2 | x <- xs, x > 3 ]
three xs = map (+2) (filter (>3) xs)
-- >>> three [1,2,3,4,5,6]
-- >>> threeList [1,2,3,4,5,6]
-- [6,7,8]
-- [6,7,8]
--

fourList xs = [ x+3 | (x,_) <- xs ]
four xs = map (\x -> fst x + 3) xs
-- >>> four [(1,2),(3,4)]
-- >>> fourList [(1,2),(3,4)]
-- [4,6]
-- [4,6]
--

fiveList xs = [ x+4 | (x,y) <- xs, x+y < 5 ]
five xs = map (\x -> fst x+4) (filter (\x -> fst x + snd x < 5) xs)
-- >>> five [(1,2),(3,4)]
-- >>> fiveList [(1,2),(3,4)]
-- [5]
-- [5]
--

sixList mxs = [ x+5 | Just x <- mxs ]
-- >>> sixList
-- <interactive>:44:2: error:
--     * No instance for (Show ([Maybe Integer] -> [Integer]))
--         arising from a use of `print'
--         (maybe you haven't applied a function to enough arguments?)
--     * In a stmt of an interactive GHCi command: print it
--
